import request from '@/utils/request'

export function getH5Page(key) {
    return request({
      url: '/chenzhi/h5-page/' + key,
      method: 'get'
    })
  }

  export function updateH5Page(data) {
    return request({
      url: '/chenzhi/h5-page',
      method: 'put',
      data: data
    })
  }
  