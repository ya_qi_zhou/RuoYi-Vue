import request from '@/utils/request'

export function getCarousel(id) {
    return request({
        url: '/chenzhi/carousel/getCarousel?id=' + id,
        method: 'get'
    })
}

export function listCarousel(query) {
    return request({
        url: '/chenzhi/carousel/selectByType',
        method: 'get',
        params: query
    })
}

export function addCarousel(data) {
    return request({
        url: '/chenzhi/carousel/addCarousel',
        method: 'post',
        data: data
    })
}

export function editCarousel(data) {
    return request({
        url: '/chenzhi/carousel/editCarousel',
        method: 'post',
        data: data
    })
}

export function removeCarousel(id) {
    return request({
        url: '/chenzhi/carousel/removeCarousel?id=' + id,
        method: 'delete'
    })
}

export function getTypeList(query) {
    return request({
        url: '/chenzhi/carousel/getTypeList',
        method: 'get',
        params: query
    })
}

export function addType(data) {
    return request({
        url: '/chenzhi/carousel/addType',
        method: 'post',
        data: data
    })
}
