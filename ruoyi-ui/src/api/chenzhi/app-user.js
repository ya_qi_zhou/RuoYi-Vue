import request from '@/utils/request'

export function listUser(query) {
    return request({
        url: '/chenzhi/appUser/list',
        method: 'get',
        params: query
    })
}
