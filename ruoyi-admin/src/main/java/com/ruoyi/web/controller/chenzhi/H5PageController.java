package com.ruoyi.web.controller.chenzhi;


import com.ruoyi.chenzhi.domain.H5Page;
import com.ruoyi.chenzhi.service.H5PageService;
import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("chenzhi/h5-page")
public class H5PageController {

    @Autowired
    private H5PageService h5PageService;


    @GetMapping("/{id}")
    public AjaxResult aboutWe(@PathVariable("id")String id) {
        H5Page page = h5PageService.getById(id);
        return AjaxResult.success(page);
    }

    @PutMapping
    public AjaxResult edit(@RequestBody H5Page page){
        h5PageService.updateById(page);
        return AjaxResult.success();
    }
}
