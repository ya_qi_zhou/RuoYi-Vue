package com.ruoyi.web.controller.chenzhi;

import com.github.pagehelper.PageHelper;
import com.ruoyi.chenzhi.domain.AppUser;
import com.ruoyi.chenzhi.service.AppUserService;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.ruoyi.common.core.controller.BaseController.toTableDataInfo;

/**
 * @author yangd
 * @since 2024/2/9 22:41
 */
@RestController
@RequestMapping("chenzhi/appUser")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    @GetMapping("/list")
    public TableDataInfo list(AppUser appUser) {

        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();

        PageHelper.startPage(pageNum, pageSize, "create_time desc");
        List<AppUser> list = appUserService.listUser(appUser);

        return toTableDataInfo(list);
    }
}
