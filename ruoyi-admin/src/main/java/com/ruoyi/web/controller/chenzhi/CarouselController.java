package com.ruoyi.web.controller.chenzhi;

import com.ruoyi.chenzhi.domain.Carousel;
import com.ruoyi.chenzhi.domain.CarouselType;
import com.ruoyi.chenzhi.service.CarouselService;
import com.ruoyi.chenzhi.service.CarouselTypeService;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.Seq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author yangd
 * @since 2024/2/9 13:04
 */
@RestController
@RequestMapping("chenzhi/carousel")
public class CarouselController {

    @Autowired
    private CarouselService carouselService;
    @Autowired
    private CarouselTypeService carouselTypeService;



    @GetMapping("/getCarousel")
    public AjaxResult getCarousel(@RequestParam Long id) {
        Carousel carousel = carouselService.getById(id);
        return AjaxResult.success(carousel);
    }

    @GetMapping("/selectByType")
    public AjaxResult selectByType(@RequestParam String type) {

        List<Carousel> list = carouselService.selectByType(type);

        return AjaxResult.success(list);
    }

    @PostMapping("/addCarousel")
    public AjaxResult addCarousel(@Validated @RequestBody Carousel carousel){
        carouselService.save(carousel);
        return AjaxResult.success();
    }

    @PostMapping("/editCarousel")
    public AjaxResult editCarousel(@Validated @RequestBody Carousel carousel){
        carouselService.updateById(carousel);
        return AjaxResult.success();
    }

    @DeleteMapping("/removeCarousel")
    public AjaxResult removeCarousel(@RequestParam Long id){
        carouselService.removeById(id);
        return AjaxResult.success();
    }


    @GetMapping("/getTypeList")
    public AjaxResult getTypeList() {

        List<CarouselType> list = carouselTypeService.getTypeList();

        return AjaxResult.success(list);
    }

    @PostMapping("/addType")
    public AjaxResult addType(@Validated @RequestBody CarouselType carouselType) {
        if (StringUtils.isBlank(carouselType.getType())){
            String id = Seq.getId();
            carouselType.setType(id);
        }
        try {
            carouselTypeService.save(carouselType);
        }catch (DuplicateKeyException e){
            return AjaxResult.error("唯一标识重复");
        }
        return AjaxResult.success();
    }
}
