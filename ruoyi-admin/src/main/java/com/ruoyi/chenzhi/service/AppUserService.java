package com.ruoyi.chenzhi.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.chenzhi.domain.AppUser;
import com.ruoyi.chenzhi.mapper.AppUserMapper;
import com.ruoyi.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yangd
 * @since 2024/2/9 22:38
 */
@Slf4j
@Service
public class AppUserService extends ServiceImpl<AppUserMapper, AppUser> {

    public List<AppUser> listUser(AppUser appUser) {
        LambdaQueryWrapper<AppUser> query = new LambdaQueryWrapper<>();
//        query.ne(AppUser::getUserType,"00");
        query.like(StringUtils.isNotBlank(appUser.getNickName()),AppUser::getNickName,appUser.getNickName());
        query.like(StringUtils.isNotBlank(appUser.getPhonenumber()),AppUser::getPhonenumber,appUser.getPhonenumber());
        return list(query);
    }
}
