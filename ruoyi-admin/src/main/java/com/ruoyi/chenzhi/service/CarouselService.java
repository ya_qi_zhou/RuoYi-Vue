package com.ruoyi.chenzhi.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.chenzhi.domain.Carousel;
import com.ruoyi.chenzhi.mapper.CarouselMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yangd
 * @since 2024/2/9 13:03
 */
@Service
public class CarouselService extends ServiceImpl<CarouselMapper, Carousel> {


    public List<Carousel> selectByType(String type) {
        LambdaQueryWrapper<Carousel> query = new LambdaQueryWrapper<>();
        query.eq(Carousel::getType,type);
        query.orderByAsc(Carousel::getDataSort);
        return list(query);
    }
}
