package com.ruoyi.chenzhi.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.chenzhi.domain.CarouselType;
import com.ruoyi.chenzhi.mapper.CarouselTypeMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yangd
 * @since 2024/2/9 13:02
 */
@Slf4j
@Service
public class CarouselTypeService extends ServiceImpl<CarouselTypeMapper, CarouselType> {

    public List<CarouselType> getTypeList() {
        return list();
    }
}
