package com.ruoyi.chenzhi.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.chenzhi.domain.H5Page;
import com.ruoyi.chenzhi.mapper.H5PageMapper;
import org.springframework.stereotype.Service;

@Service
public class H5PageService extends ServiceImpl<H5PageMapper,H5Page> {

}
