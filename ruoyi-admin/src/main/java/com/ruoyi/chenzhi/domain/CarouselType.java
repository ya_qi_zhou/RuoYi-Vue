package com.ruoyi.chenzhi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author yangd
 * @since 2024/2/9 13:00
 */
@Data
@TableName("carousel_type")
public class CarouselType {

    @TableId(type = IdType.INPUT)
    private String type;

    @NotBlank(message = "分类名称不可为空")
    private String name;

}
