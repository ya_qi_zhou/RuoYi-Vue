package com.ruoyi.chenzhi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author yangd
 * @since 2024/2/9 22:33
 */
@Data
@TableName("sys_user")
public class AppUser {

    @TableId(value = "user_id",type = IdType.AUTO)
    private Long userId;
    /**
     * 名字
     */
    @TableField("nick_name")
    private String nickName;
    /**
     * 电话
     */
    private String phonenumber;

    private String avatar;
    /**
     * 无
     * 一级分销
     * 二级分销
     * 禁用
     */
    private String distribution;

    @TableField("create_time")
    private Date createTime;

    @TableField("user_type")
    private String userType;
}
