package com.ruoyi.chenzhi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "h5_page")
public class H5Page {

    @TableId(type = IdType.INPUT)
    private String id;

    private String title;

    private String content;
}
