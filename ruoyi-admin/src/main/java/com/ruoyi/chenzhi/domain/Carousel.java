package com.ruoyi.chenzhi.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author yangd
 * @since 2024/2/9 12:58
 */
@Data
@TableName("carousel")
public class Carousel {
    @TableId(type = IdType.AUTO)
    private Long id;

    private String type;
    @NotBlank(message = "名称不可为空")
    private String name;
    @NotBlank(message = "图片地址不可为空")
    private String picture;

    private String path;

    private Long dataSort;


}
