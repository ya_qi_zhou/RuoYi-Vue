package com.ruoyi.chenzhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.chenzhi.domain.AppUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yangd
 * @since 2024/2/9 22:38
 */
@Mapper
public interface AppUserMapper extends BaseMapper<AppUser> {
}
