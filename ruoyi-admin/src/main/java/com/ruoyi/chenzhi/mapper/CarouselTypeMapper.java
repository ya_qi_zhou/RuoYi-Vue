package com.ruoyi.chenzhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.chenzhi.domain.CarouselType;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yangd
 * @since 2024/2/9 13:02
 */
@Mapper
public interface CarouselTypeMapper extends BaseMapper<CarouselType> {
}
