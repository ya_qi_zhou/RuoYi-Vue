package com.ruoyi.chenzhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.chenzhi.domain.Carousel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yangd
 * @since 2024/2/9 13:03
 */
@Mapper
public interface CarouselMapper extends BaseMapper<Carousel> {
}
