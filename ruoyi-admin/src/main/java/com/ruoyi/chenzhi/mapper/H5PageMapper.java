package com.ruoyi.chenzhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.chenzhi.domain.H5Page;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface H5PageMapper extends BaseMapper<H5Page> {
}
