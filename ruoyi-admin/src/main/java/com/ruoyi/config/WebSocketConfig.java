package com.ruoyi.config;

import com.ruoyi.handler.CustomHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
public class WebSocketConfig implements WebSocketConfigurer {
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        // 注册WebSocket处理器，并指定处理器路径
        registry.addHandler(new CustomHandler(), "/websocket")
                // 允许跨域访问
                .setAllowedOrigins("*");
    }

    @Scheduled(fixedDelay = 5000) // 定时任务，每5秒发送一次心跳消息
    public void sendHeartbeat() {
        CustomHandler.sendHeartbeatToAllClients();
    }
}
