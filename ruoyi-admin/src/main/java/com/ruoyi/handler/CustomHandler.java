package com.ruoyi.handler;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class CustomHandler extends TextWebSocketHandler {

    private static Set<WebSocketSession> sessions = new HashSet<>();



    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        sessions.add(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        sessions.remove(session);
    }

    public static void sendHeartbeatToAllClients(){

        Iterator<WebSocketSession> it = sessions.iterator();

        while (it.hasNext()){
            try {
                WebSocketSession next = it.next();
                next.sendMessage(ChatMessage.toBeat());
            }catch (Exception e){
                System.out.println("该Session已经断开");
                it.remove();
            }
        }
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message)
            throws Exception {
        // 处理接收到的消息
        ChatMessage chat = ChatMessage.convert(message);
        if (chat == null || "heartbeat".equals(chat.getText())){
            //心跳包
        }else {
            System.out.println("Received message: " + chat);
            // 发送消息给所有连接的客户端
            String headerValue = session.getHandshakeHeaders().get("sec-websocket-key").get(0);
            System.out.println(headerValue);
            session.sendMessage(ChatMessage.toJson("Hello, client!"));
        }
    }
}
