package com.ruoyi.handler;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.utils.uuid.Seq;
import lombok.Data;
import org.springframework.web.socket.TextMessage;

@Data
public class ChatMessage {

    private String id;

    private String text;

    public ChatMessage() {
    }

    public ChatMessage(String text) {
        this.id = Seq.getId();
        this.text = text;
    }

    public static TextMessage toBeat(){
        ChatMessage message = new ChatMessage("heartbeat");
        return new TextMessage(JSON.toJSONString(message));
    }

    public static TextMessage toJson(String text){
        ChatMessage message = new ChatMessage(text);
        return new TextMessage(JSON.toJSONString(message));
    }

    public static ChatMessage convert(TextMessage textMessage){
        String payload = textMessage.getPayload();
        if (JSON.isValid(payload)){
            return JSON.parseObject(payload,ChatMessage.class);
        }
        return null;
    }
}
