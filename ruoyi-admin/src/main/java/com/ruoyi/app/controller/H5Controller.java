package com.ruoyi.app.controller;

import cn.hutool.core.lang.Assert;
import com.ruoyi.chenzhi.domain.H5Page;
import com.ruoyi.chenzhi.service.H5PageService;
import com.ruoyi.common.annotation.Anonymous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/app/h5")
public class H5Controller {

    @Autowired
    private H5PageService h5PageService;

    @Anonymous
    @GetMapping("/")
    public String getH5(Model model, String id){
        H5Page page = h5PageService.getById(id);
        Assert.notNull(page,"页面不存在");
        model.addAttribute("title",page.getTitle());
        model.addAttribute("content",page.getContent());
        return "h5Page";
    }
}
