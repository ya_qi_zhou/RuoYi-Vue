package com.ruoyi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RedisConnectionMonitor {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Scheduled(fixedDelay = 1000)
    public void checkConnection(){
        try {
            RedisConnection conn = redisTemplate.getConnectionFactory().getConnection();

            conn.ping();

//            System.out.println("Redis connection is healthy");

        }catch (Exception e){
            System.out.println("Redis connection is disconnected. Reconnecting...");
        }
    }
}
